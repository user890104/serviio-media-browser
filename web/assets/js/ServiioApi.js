'use strict';

(function(ctx) {
	var prefsKey = 'ServiioPrefs_v1';
	var prefs = {};
	var config = {};
	
	function savePrefs() {
		try {
			localStorage.setItem(prefsKey, JSON.stringify(prefs));
		}
		catch (e) {}
	}
	
	function loadPrefs() {
		var data = localStorage.getItem(prefsKey);
		
		if (data === null) {
			return;
		}
		
		try {
			prefs = JSON.parse(data);
		}
		catch (e) {}
	}
	
	function signPassword(text, key) {
		var shaObj = new jsSHA('SHA-1', 'TEXT');
		shaObj.setHMACKey(key, 'TEXT');
		shaObj.update(text);
		return shaObj.getHMAC('B64');
	}
	
	function buildAuthHeader(signature) {
		return ['Authorization', 'Serviio ' + signature];
	}
	
	function setProfile(profileId) {
		var found = false;

		$.each(config.profiles, function(idx, profile) {
			if (profile.id === profileId) {
				config.activeProfile = profile.id;
				
				console.info('Switched to profile ' + config.activeProfile);
				found = true;
				return false;
			}
		});
		
		return found;
	}
	
	function getLastRenderer() {
		return prefs.renderer || '';
	}
	
	function setRenderer(rendererUuid) {
		prefs.renderer = rendererUuid;
		savePrefs();
	}
	
	function checkAuthToken(authToken) {
		return browseMetadata(null, {
			authToken: authToken
		});
	}
	
	// Generic API call
	function apiCall(path, opts) {
		var params = {
			url: config.baseUrl + '/cds/' + path,
			headers: [
				['Accept', 'application/json, text/javascript, */*']
			]
		};
		
		if (!opts) {
			opts = {};
		}
		
		if (!opts.query) {
			opts.query = {};
		}
		
		if (opts.authToken) {
			opts.query.authToken = opts.authToken;
		}
		
		if (prefs.authToken && opts.authToken !== false) {
			opts.query.authToken = prefs.authToken;
		}
		
		if (opts.method) {
			params.method = opts.method;
		}
		
		if (opts.query) {
			params.url += '?' + $.param(opts.query);
		}
		
		if (opts.data) {
			params.headers = params.headers.concat([
				['Content-Type', 'application/json']
			]);
			
			params.data = JSON.stringify(opts.data);
		}
		
		if (opts.headers) {
			params.headers = params.headers.concat(opts.headers);
		}
		
		return $.xhrFetch(params).then(function(data) {
			return JSON.parse(data);
		});
	}
	
	// Wrappers
	function browseWrapper(method, browseOpts, apiOpts) {
		if (!browseOpts) {
			browseOpts = {};
		}
		
		var path = 'browse/' +
			config.activeProfile + '/' +
			(browseOpts.id ? String(browseOpts.id) : '0') + '/' +
			method + '/' +
			(browseOpts.filter || 'all') + '/' +
			(browseOpts.startIndex ? String(browseOpts.startIndex) : '0') + '/' +
			(browseOpts.requestedCount ?
				String(browseOpts.requestedCount) :
				(method === 'BrowseMetadata' ? '1' : '0')
			);
		
		return apiCall(path, apiOpts).then(function(response) {
			return response.objects;
		});
	}
	
	
	// API methods
	function login(password) {
		var date = new Date;
		var dateStr = date.toUTCString();
		var signature = signPassword(dateStr, password);
		
		console.info('Logging in using password...');
		
		return apiCall('login', {
			method: 'POST',
			headers: [
				['X-Serviio-Date', dateStr],
				buildAuthHeader(signature)
			]
		}).then(function(response) {
			if (response.errorCode) {
				return Promise.reject(new Error('Login failed'));
			}
			
			prefs.authToken = response.parameter[0];
			savePrefs();
			console.info('Logged in successfully, authToken=', prefs.authToken);
			return Promise.resolve();
		});
	}
	
	function browseMetadata(browseOpts, apiOpts) {
		return browseWrapper('BrowseMetadata', browseOpts, apiOpts);
	}
	
	function browseDirectChildren(browseOpts, apiOpts) {
		return browseWrapper('BrowseDirectChildren', browseOpts, apiOpts);
	}
	
	function getRenderers(apiOpts) {
		return apiCall('renderers', apiOpts).then(function(response) {
			return response.renderers;
		});
	}
	
	function controlRequest(uuid, action, controlOpts) {
		var data = {
			action: action
		};
		
		if (action === 'play') {
			if ('objectId' in controlOpts) {
				data.objectId = controlOpts.objectId;
			}
			
			if ('parentId' in controlOpts) {
				data.parentId = controlOpts.parentId;
			}
		}
		
		return apiCall('control/' + uuid, {
			method: 'POST',
			data: data
		});
	}
	
	function play(uuid, objectId, parentId) {
		return controlRequest(uuid, 'play', {
			objectId: objectId,
			parentId: parentId
		});
	}
	
	function resume(uuid) {
		return controlRequest(uuid, 'resume');
	}
	
	function stop(uuid) {
		return controlRequest(uuid, 'stop');
	}
	
	function pause(uuid) {
		return controlRequest(uuid, 'pause');
	}
	
	function checkLogin() {
		if (config.app.cdsAnonymousEnabled) {
			console.info('Anonymous access enabled');
			return Promise.resolve();
		}
		
		console.info('Anonymous access disabled');
		
		if ('authToken' in prefs) {
			console.info('Checking cached auth token...');
			
			return checkAuthToken(prefs.authToken).then(function() {
				console.info('Cached auth token is still valid!');
				return Promise.resolve();
			}).caught(function() {
				console.info('Cached token is not valid');
				return Promise.reject(new Error('Cached token is not valid'));
			});
		}
		
		return Promise.reject(new Error('No cached auth token found'));
	}
	
	// Constructor
	function ServiioApi(baseUrl) {
		loadPrefs();
		
		config.baseUrl = baseUrl || '';
		console.info('Connecting to Serviio...');
	
		var api = apiCall('ping', {
			authToken: false
		}).then(function(response) {
			if (response.errorCode) {
				return Promise.reject(new Error('Unable to connect to Serviio, code=' + response.errorCode));
			}
			
			console.info('Connected!');
			return apiCall('application', {
				authToken: false
			});
		}).then(function(response) {
			config.app = response;
			
			console.info('Serviio ' + config.app.version + ' ' + config.app.edition);
			console.info('License: ' + config.app.license.type + ', expires in ' + config.app.license.expiresInMinutes + ' minutes');
			
			return apiCall('profiles', {
				authToken: false
			});
		}).then(function(response) {
			config.profiles = response.profiles.filter(function (item) {
				return !item.selectable;
			});
			
			return Promise.resolve({
				profileFound: setProfile('webplayer_hls'),
			});
		});
		
		return {
			init: function() {
				return api;
			},
			getProfiles: function() {
				return config.profiles;
			},
			setProfile: setProfile,
			getUrl: function(path) {
				return config.baseUrl + path + (path.indexOf('?') === -1 ? '?' : '&') + $.param({
					authToken: prefs.authToken
				});
			},
			call: apiCall,
			checkLogin: checkLogin,
			login: login,
			browseMetadata: browseMetadata,
			browseDirectChildren: browseDirectChildren,
			getRenderers: getRenderers,
			getLastRenderer: getLastRenderer,
			setRenderer: setRenderer,
			play: play,
			resume: resume,
			stop: stop,
			pause: pause
		};
	}
	
	ctx.ServiioApi = ServiioApi;
})(window);
