'use strict';

(function($) {
	function xhrFetch(opts) {
		return new Promise(function (resolve, reject) {
			var xhr = new XMLHttpRequest();
			var isRunning = false;
			
			xhr.addEventListener('error', function() {
				reject(new Error('XHR error'));
			});
			
			xhr.addEventListener('readystatechange', function(e) {
				if (xhr.readyState === 3) {
					isRunning = true;
				}
				
				if (xhr.readyState !== 4) {
					return;
				}

				if (xhr.status === 0 && isRunning) {
					reject(new Promise.OperationalError('XHR aborted'));
				}
				
				if (xhr.status !== 200) {
					reject(new Error('XHR failed, status=' + xhr.status));
				}
				
				resolve(xhr.response);
			});

			xhr.open(opts.method || 'GET', opts.url);
			
			if (opts.headers) {
				$.each(opts.headers, function() {
					xhr.setRequestHeader(this[0], this[1]);
				});
			}
			
			xhr.send(opts.data || null);
		});
	}
	
	$.xhrFetch = xhrFetch;
})(jQuery);
