'use strict';

jQuery(function($) {
	var hls, mouseTimer;
	var player = $('#player');

	function attachAndPlay(video, url) {
		hls = new Hls({
			//debug: true,
			maxMaxBufferLength: 30,
			maxBufferSize: 20000000
		});

		hls.on(Hls.Events.ERROR, function (event, data) {
			if (!data.fatal) {
				return;
			}

			switch(data.type) {
				case Hls.ErrorTypes.NETWORK_ERROR:
					// try to recover network error
					console.log('fatal network error encountered, try to recover');
					hls.startLoad();
					break;
				case Hls.ErrorTypes.MEDIA_ERROR:
					console.log('fatal media error encountered, try to recover');
					hls.recoverMediaError();
					break;
				default:
					// cannot recover
					hls.destroy();
					break;
			}
		});

		hls.on(Hls.Events.MANIFEST_PARSED, function() {
			video.play();
		});

		hls.on(Hls.Events.MEDIA_ATTACHED, function () {
			hls.loadSource(url);
		});

		hls.attachMedia(video);
	}

	function makeIcon(id, isListItem) {
		return $('<i />').addClass('fa' + (isListItem ? ' fa-li' : '') + ' fa-' + id).attr({
			'aria-hidden': 'true'
		});
	}
	
	function makeButton(iconId, action) {
		return $('<a />').attr({
			href: '#'
		}).addClass('control-button').data({
			action: action
		}).append(makeIcon(iconId));
	}

	$.fn.setIcon = function(id) {
		return $(this).removeClass(function(idx, classes) {
			return classes.split(' ').filter(function(className) {
				return className.indexOf('fa-') === 0 && ['fa-ul', 'fa-li'].indexOf(className) === -1;
			}).join(' ');
		}).addClass('fa-' + id);
	};

	function renderChildren(parent, children) {
		var list = $('<ul />').addClass('fa-ul').hide();

		$.each(children, function(idx, child) {
			var title = child.title || (child.quality + ' (' + child.resolution + ')');
			var iconId;

			switch (child.type) {
				case 'CONTAINER':
					title += ' (' + child.childCount + ')';
					iconId = 'folder-o';
				break;
				case 'ITEM':
					iconId = 'file-o';
				break;
				default:
					iconId = 'puzzle-piece';
				break;
			}

			var link = $('<a />').attr({
				href: '#'
			}).text(title).data('info', child);
			
			if ('description' in child) {
				link.attr('title', child.description);
			}

			if ('url' in child) {
				link.addClass('type-content');
			}
			
			['type', 'fileType', 'contentType'].forEach(function(prop) {
				if (prop in child) {
					link.addClass(prop.toLowerCase() + '-' + child[prop].toLowerCase());
				}
			});

			var listItem = $('<li />');

			if (iconId) {
				listItem.append(makeIcon(iconId, true));
			}
			
			listItem.append(link);
			
			if ('subtitlesUrl' in child) {
				listItem.append($('<span>').text(' '), makeIcon('commenting-o'));
			}
			
			if (child.type === 'ITEM') {
				listItem.append($('<span>').text(' '), $('<label>').addClass('upload-subs').append(makeIcon('plus-circle'), makeIcon('file-text'), $('<input>').attr({
					type: 'file'
				})));
			}
			
			list.append(listItem);
		});

		list.appendTo(parent).slideDown(250);
	}
	
	function renderRenderer(info) {
		var lastRenderer = api.getLastRenderer();

		return $('<li />').data('info', info).append(
			makeIcon('tv', true),
			$('<div />').append(
				$('<label />').text(info.name + ' (' + info.ipAddress + ')').prepend(
					$('<input />').attr({
						type: 'radio',
						name: 'renderer',
						value: info.uuid
					}).prop('checked', info.uuid === lastRenderer)
				)
			),
			info.uuid.length > 0 ? $('<div />').append(
				makeButton('play', 'resume'),
				makeButton('pause', 'pause'),
				makeButton('stop', 'stop')
			) : null
		)
	}
	
	function renderRenderers(parent, children) {
		var list = $('<ul />').addClass('fa-ul').hide();
		
		renderRenderer({
			name: 'Web renderer',
			ipAddress: 'localhost',
			uuid: ''
		}).appendTo(list);
		
		$.each(children, function(idx, child) {
			list.append(renderRenderer(child));
		});
		
		parent.children('ul').remove();
		list.appendTo(parent).slideDown(250);
	}
	
	function refreshRenderers() {
		return api.getRenderers().then(function(renderers) {
			var container = $('#renderers')
			renderRenderers(container, renderers);
			container.show();
		});
	}
	
	function convertSrtToVtt(srt) {
		var parts = [
			'WEBVTT\n\n',
			srt
				.replace(/(\d\d:\d\d:\d\d),(\d\d\d)/g, '$1.$2')
				.replace(/^\d+\r?\n/gm, '')
		];

		return new Blob(parts, {
			type: 'text/vtt; charset=utf-8'
		});
	}
	
	function attachTextTrack(player, subsText, label) {
		var vtt = convertSrtToVtt(subsText);
		var url = URL.createObjectURL(vtt);
		
		player.append($('<track />').attr({
			kind: 'subtitles',
			label: label || 'Captions',
			src: url
		}));
	}
	
	$(document).on('click', '#tree ul li a', function(e) {
		e.preventDefault();

		var $this = $(this);
		
		if ($this.hasClass('state-disabled')) {
			return;
		}

		if ($this.hasClass('state-loaded')) {
			var list = $this.siblings('ul');
			
			if ($this.hasClass('type-container')) {
				$this.prev('i.fa').setIcon(list.is(':visible') ? 'folder-o' : 'folder-open-o');
			}
			
			list.slideToggle(250);
			return;
		}

		if ($this.hasClass('type-container')) {
			$this.addClass('state-disabled');

			var icon = $this.prev('i.fa');
			icon.setIcon('spinner fa-spin');

			api.browseDirectChildren({
				id: $this.data('info').id
			}).then(function(objects) {
				$this.addClass('state-loaded');
				
				if ($this.siblings('i.fa-refresh').length === 0) {
					$this.after(makeIcon('refresh'));
				}
				
				icon.setIcon('folder-open-o');
				renderChildren($this.parent(), objects);
			}).caught(function() {
				icon.setIcon('folder-o');
			}).lastly(function() {
				$this.removeClass('state-disabled');
			});
		}

		if ($this.hasClass('type-item')) {
			$this.addClass('state-loaded');
			var info = $this.data('info');
			renderChildren($this.parent(), info.contentUrls);
		}

		if ($this.hasClass('type-content')) {
			var $this = $(this);
			var item = $this.closest('ul').siblings('a.type-item');
			var itemInfo = item.data('info');

			var lastRenderer = api.getLastRenderer();
			
			if (lastRenderer.length > 0) {
				// Remote rendering
				api.play(lastRenderer, itemInfo.id, itemInfo.parentId);
				return;
			}
			
			// Local rendering
			if (!Hls.isSupported()) {
				alert('HLS not supported!');
				return;
			}
			
			if (hls) {
				hls.destroy();
			}

			var offset = player.removeClass('hidden').offset();
			$(document.body).scrollTop(offset.top);
			var info = $this.data('info');
			player.children('track[kind="subtitles"]').remove();

			var uploadedSubs = item.siblings('.upload-subs').children('input[type="file"]').get(0).files;			
			var hasRemoteSubs = ('subtitlesUrl' in itemInfo);
			var hasUploadedSubs = uploadedSubs.length > 0;
			
			var deffereds = [];

			if (hasRemoteSubs) {
				deffereds.push($.ajax(api.getUrl(itemInfo.subtitlesUrl)).then(function(result) {
					attachTextTrack(player, result, 'Remote captions');
				}));
			}
			
			if (hasUploadedSubs) {
				var reader = new FileReader;
				var def = $.Deferred();
				deffereds.push(def);
				reader.addEventListener('load', function() {
					attachTextTrack(player, reader.result, 'Uploaded captions');
					def.resolve();
				});
				reader.readAsText(uploadedSubs[0]);
			}
			
			$.when.apply($, deffereds).then(function() {
				attachAndPlay(player.get(0), api.getUrl(info.url));
			});
		}
	}).on('click', '#tree ul li i.fa-refresh', function(e) {
		var $this = $(this);
		$this.siblings('ul').remove()
		$this.siblings('a').removeClass('state-loaded').trigger('click');
	}).on({
		click: function(e) {
			if (navigator.userAgent.indexOf('Firefox/') > -1) {
				return;
			}
			
			e.target[e.target.paused ? 'play' : 'pause']();
		},
		dblclick: function(e) {
			if (document.fullscreenElement) {
				document.exitFullscreen();
			}
			else {
				e.target.requestFullscreen();
			}
		}
	}, '#player').on('mousemove', function() {
		if (mouseTimer) {
			clearTimeout(mouseTimer);
		}
		
		mouseTimer = setTimeout(function() {
			player.addClass('hide-ui');
		}, 3000);
		
		player.removeClass('hide-ui');
	}).on('change', '#renderers input:radio', function() {
		api.setRenderer($(this).val());
	}).on('click', '#renderers .control-button', function(e) {
		e.preventDefault();
		var $this = $(this);
		var action = $this.data('action');
		var info = $this.closest('li').data('info');
		
		if (!action || !info) {
			return;
		}
		
		api[action](info.uuid);
	}).on('click', '.refresh-renderers', function(e) {
		e.preventDefault();
		refreshRenderers();
	}).on('change', '.upload-subs input[type="file"]', function(e) {
		var container = $(this).closest('.upload-subs');
		
		if (this.files.length === 0) {
			container.siblings('.fa-commenting').next('span').addBack().remove();
			return;
		}
		
		container.before(makeIcon('commenting').attr('title', this.files[0].name), $('<span>').text(' '));
	});

	var api = new ServiioApi();

	api.init().then(function(state) {
		return api.checkLogin().caught(function() {
			$('#loading').hide();
			$('#loginBox').show();

			return new Promise(function(resolve, reject) {
				$('#login').on('click', function() {
					resolve(api.login($('#password').val()).caught(function() {
						alert('Wrong password');
					}));
				});
			});
		});
	}).then(function() {
		console.log('Logged in!');
		$('#loading,#loginBox').hide();

		return api.browseDirectChildren().then(function(objects) {
			var container = $('#tree');
			renderChildren(container, objects);
			container.show();
		});
	}).then(refreshRenderers);
});
